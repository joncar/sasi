<!-- Top shadow -->
<div class="shadow"></div>
<!-- end top shadow -->

<!-- The splash screen -->
<div id="splash">
    <div class="loader">
        <img class="splash-logo" src="<?= base_url() ?>img/logo/logo.svg" />
        <div class="line"></div>
    </div>
</div>
<!-- End of splash screen -->

<div id="wrapper">
    <!-- main content -->
    <main>
        <!-- The header for content -->
        <header class="detail">
            <a href="<?= site_url() ?>" class="back" data-transition="slide-from-top">
                <h1>tornar</h1>
            </a>
            <section>
                <h3 class="badge">Contacto</h3>
                <h1>Deja tu mensaje</h1>
            </section>
        </header>
        <!-- end header -->
        <div class="content-wrap">
            <section class="content">
                <i class="icon bg icon-LocationMarker1"></i>
                <section class="info">
                    <header>
                        <h2>Oficina Central</h2>
                        <h4 class="serif">de Igualada</h4>
                    </header>
                    <p>Nos encontrarás en la Avda Barcelona, 1, allí podremos atenderte y asesorarte directamente. Nuestros comerciales te ofrecerán los mejores productos para tí. No dudes en pasar a visitarnos, o si lo prefieres puedes contactar con nosotros por correo electrónico, redes sociales o teléfonicamente. Te esperamos.

</p>
                    <ul class="icon-list">
                        <li>
                            <a href="https://www.google.es/maps/place/Finques+SASI/@41.5864143,1.6123442,17z/data=!4m13!1m7!3m6!1s0x12a469e114031137:0x826cdb63ff5eb73b!2sAv.+de+Barcelona,+1,+08700+Igualada,+Barcelona!3b1!8m2!3d41.5864143!4d1.6145329!3m4!1s0x12a469dcaf088c55:0xd90aff7f006a5ea7!8m2!3d41.586414!4d1.614532">
                                <i class="icon icon-LocationMarker"></i>
                                <span>Av. Barcelona, 1. 08700 Igualada (Barcelona) </span>
                            </a>
                        </li>
                        <li>
                            <a href="tel:+34938048171">
                                <i class="icon icon-Phone"></i>
                                <span>+34 938 048 171</span>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:info@finques-sasi.com">
                                <i class="icon icon-Email"></i>
                                <span>info@finques-sasi.com</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/finquessasiigualada/">
                                <i class="icon icon-Share"></i>
                                <span>facebook/finquessasiigualada</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <section class="hours">
                    <header>
                        <h2>Horarios</h2>
                        <h4 class="serif">Atención al público</h4>
                    </header>
                    <p style="text-align:center">Este es el horario de todas nuestras Oficinas. </p>
                    <div class="hours">
                        <time itemprop="openingHours" datetime="Monday 10:00-24:00">
                            <strong>Lunes</strong>
                            <h3>9.30 - 13:00 / 16:30 a 20:30</h3>
                        </time>
                        <time itemprop="openingHours" datetime="Tuesday 10:00-24:00">
                            <strong>Martes</strong>
                            <h3>9.30 - 13:00 / 16:30 a 20:30</h3>
                        </time>
                        <time itemprop="openingHours" datetime="Wednesday 10:00-24:00">
                            <strong>Miércoles</strong>
                            <h3>9.30 - 13:00 / 16:30 a 20:30</h3>
                        </time>
                        <time itemprop="openingHours" datetime="Closed">
                            <strong>Jueves</strong>
                            <h3>9.30 - 13:00 / 16:30 a 20:30</h3>
                        </time>
                        <time itemprop="openingHours" datetime="Friday 10:00-24:00">
                            <strong>Viernes</strong>
                            <h3>9.30 - 13:00 / 16:30 a 20:30</h3>
                        </time>
                        <time itemprop="openingHours" datetime="Saturday 10:00-23:00">
                            <strong>Sádado</strong>
                            <h3>10.00 - 13:00 </h3>
                        </time>
                        <time itemprop="openingHours" datetime="Sunday closed">
                            <strong>Domingo</strong>
                            <h3>Cerrado</h3>
                        </time>
                    </div>
                </section>
                <section class="form-inline">
                    <header>
                        <h2>Contactar via e-mail</h2>
                        <h4 class="serif">Nos gusta saber de ti</h4>
                    </header>
                    <p>Si prefieres contactar a través de este formulario, rellena todas las casillas y escribe tu consulta, te contestaremos a la mayor brevedad posible. Te llamamos en menos de 24 horas.</p>
                    <form action="./php/contact.php" method="post" id="contact-form" class="form ambiance-html-form">
                        <div class="row">
                            <div class="form-group">
                                <input name="name" id="name" type="text" placeholder="nombre">
                                <i class="icon icon-User"></i>
                            </div>
                            
                            <div class="form-group">
                                <input class="full-border" name="telefon" id="telefon" type="text" placeholder="teléfono">
                                <i class="icon icon-iPad"></i>
                            </div>
                            <div class="form-group fullwidth">
                                <input class="full-border" name="email" id="email" type="email" placeholder="email">
                                <i class="icon icon-Email"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea rows="10" cols="40" required="required" name="message" id="body" placeholder="mensaje"></textarea>
                            <i class="icon icon-Typing"></i>
                        </div>
                        <span class="message"><strong>Estado.</strong> idle</span>
                        <div class="submit">
                            <button type="submit" value="submit">
                                <i class="icon icon-Forward"></i>
                            </button>
                        </div>
                    </form>
                </section>
            </section>
        </div>
        <div data-remodal-id="modal">
            <i class="icon bg icon-CommentwithLines"></i>
            <button data-remodal-action="close" class="remodal-close"></button>
            <h1>Grácias!</h1>
            <p>Nos pondremos en contancto contigo lo más pronto posible!</p>
            <div class="signature center">
                <h6>-Finques Sasi-</h6>
                <h5></h5>
            </div>
        </div>
    </main>
    <!-- end of main content -->
</div>

<!-- The slideshow -->
<ul id="slideshow" data-speed="6000">
    <li>
        <img src="<?= base_url() ?>img/slideshow/contacte.jpg" alt="slideshow image" />
    </li>
  <!-- 
  <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
 -->
</ul>
<!-- end of slideshow -->

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe.
     It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides.
        PhotoSwipe keeps only 3 of them in the DOM to save memory.
        Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>

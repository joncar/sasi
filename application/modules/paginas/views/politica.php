<!-- Top shadow -->
<div class="shadow"></div>
<!-- end top shadow -->

<!-- The splash screen -->
<div id="splash">
    <div class="loader">
        <img class="splash-logo" src="<?= base_url() ?>img/logo/logo.svg" />
        <div class="line"></div>
    </div>
</div>
<!-- End of splash screen -->

<div id="wrapper">
    <!-- main content -->
    <main>
        <!-- The header for content -->
        <header class="detail">
            <a href="<?= site_url() ?>" class="back" data-transition="slide-from-top">
                <h1>tornar</h1>
            </a>
            <section>
                <h3 class="badge">Política</h3>
                <h1>de privacidad</h1>
            </section>
        </header>
        <!-- end header -->
        <div class="content-wrap">
            <section class="content">
                <i class="icon bg icon-Notification"></i>
                <section class="info">
                    <!--<header>
                        <h2>Oficina Central</h2>
                        <h4 class="serif">de Igualada</h4>
                    </header>-->
                    <p>Mediante el presente comunicado FINQUES SASI, S.L.,  con domicilio en calle Avda Barcelona, nº1, 08700 Igualada (Barcelona), Telf.: 938048171, pone en conocimiento de los consumidores y usuarios del sitio Web www.finques-sasi.com su política de protección de datos de carácter personal, para que los usuarios determinen libre y voluntariamente si desean facilitar a FINQUES SASI S.L.  los datos personales que se les requieran para la prestación de servicios o venta de productos, oferta de presupuestos que previamente han solicitado por cualquier medio, y contestación de consultas, con ocasión de la suscripción, registro o cumplimentación de cualesquiera formulario de datos online.
                        FINQUES SASI, S.L., se reserva la facultad de modificar esta Política de Privacidad para mantenerla adaptada a la legislación vigente sobre protección de datos. En tales casos, FINQUES SASI, S.L. anunciará en este sitio Web los cambios introducidos con razonable antelación a su puesta en práctica. <br><br>
                        La visita a este sitio Web no implica que el usuario esté obligado a facilitar ninguna información sobre el mismo. En el caso de que el usuario facilite alguna información de carácter personal, los datos recogidos en este sitio Web serán tratados de forma leal y lícita con sujeción en todo momento a los principios y derechos recogidos en la <b>Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (LOPD) </b>, y demás normativa de desarrollo.
                        <br><br>
                        <b style="font-size: 16px;">El derecho de información</b><br>
                        Conforme al Art.5 de la LOPD le informamos de que los datos suministrados por usted van a pasar a formar parte del fichero Clientes/Clientes Potenciales/Usuarios Web de FINQUES SASI, S.L., encontrándose inscrito en el Registro General de Protección de Datos (RGPD), dependiente de la Agencia Española de Protección de Datos (AEPD), siendo el único destinatario de la información suministrada. La finalidad del tratamiento será la gestión y administración de la <b>cartera de clientes</b> para poder prestarles el servicio  u ofrecer el producto solicitado, y el cobro del mismo, confeccionar presupuestos y enviar comunicaciones comerciales por cualquier medio (postal o electrónico) sobre los diversos productos y servicios  que presta la empresa, consintiéndolo usted de forma expresa. El responsable del fichero es FINQUES SASI, S.L. que tiene su domicilio en Avda. Barcelona Nº1, 08700 Igualada (Barcelona), pudiendo ejercitar sus derechos de acceso, rectificación, cancelación y oposición mandando escrito a la anterior dirección con los requisitos contemplados en la Instrucción 1/1998, de la Agencia Española de Protección de Datos, relativa al ejercicio de los mencionados derechos, o personalmente en nuestra sede acreditando su identidad.
                        En el momento de proceder a la recogida de los datos se indicará el carácter voluntario u obligatorio de los datos objeto de recogida. La negativa a facilitar los datos calificados como obligatorios supondrá la imposibilidad de prestar el producto o servicio  objeto de contratación. El usuario que facilite sus datos deberá mantenerlos actualizados y comunicar al responsable del fichero cualquier cambio en sus datos. El usuario que facilite datos personales sobre otra persona, deberá él mismo, en su caso, informar al titular de los datos de lo establecido en la presente Política de Privacidad.

                        <br><br><b style="font-size: 16px;">El consentimiento informado</b><br>
                        En consecuencia, los usuarios que faciliten datos de carácter personal en este sitio Web consienten de forma inequívoca la incorporación de los mismos al fichero automatizado Clientes/Clientes Potenciales/Usuarios Web del que es responsable  para las finalidades específicamente determinadas anteriormente.

                        <br><br><b style="font-size: 16px;">El principio de calidad de datos</b><br>
                        Los datos de carácter personal proporcionados por los usuarios serán exactos y puestos al día de forma que respondan con veracidad a la situación actual del afectado. Por ello, el usuario deberá responder de la veracidad y certeza de los datos personales suministrados y comunicar cualquier modificación de los mismos que se produzca en un futuro. FINQUES SASI S.L. procederá a la cancelación de los datos personales recogidos cuando dejen de ser necesarios o pertinentes para la finalidad para la que hubieren sido recabados o registrados. La cancelación dará lugar al bloqueo de los datos, conservándose únicamente a disposición de las Administraciones públicas, Jueces y Tribunales, para la atención de las posibles responsabilidades nacidas del tratamiento, durante el plazo de prescripción de éstas. Cumplido el citado plazo se procederá a la supresión de sus datos personales.

                        <br><br><b style="font-size: 16px;">El principio de seguridad de los datos</b><br>
                        FINQUES SASI, S.L. ha adoptado todas las medidas técnicas y organizativas necesarias para garantizar la seguridad de los datos de carácter personal y evitar su alteración, pérdida, tratamiento o acceso no autorizado. El nivel de seguridad adoptado está en consonancia con la naturaleza de los datos personales suministrados.

                        <br><br><b style="font-size: 16px;">El empleo de cookies</b><br>
                        Las cookies son pequeños ficheros de datos o conjunto de caracteres que se almacenan en el disco duro o en la memoria temporal del ordenador del usuario cuando accede a las páginas de determinados sitios Web. Se utilizan para que el servidor accedido pueda conocer las preferencias del usuario al volver éste a conectarse. El acceso a este sitio Web puede implicar la utilización de cookies, que tendrán por finalidad el facilitar el proceso de compra online de los productos o servicios ofertados, y servir la publicidad o determinados contenidos o informaciones  de interés para el usuario. Las cookies utilizadas no pueden leer los archivos cookies creados por otros proveedores. El usuario tiene la posibilidad de configurar su navegador para ser avisado en pantalla de la recepción de cookies y para impedir la instalación de cookies en su disco duro. Para ello, el usuario debe consultar las instrucciones y manuales de su navegador para ampliar esta información. Ninguna cookie permite extraer información del disco duro del usuario o acceder a información personal. Simplemente asocian el navegador de un ordenador determinado a un código anónimo. La única manera de que la información privada de un usuario forme parte de un archivo cookie, es que el usuario dé personalmente esa información al servidor.

                        <br><br>El envío de comunicaciones comerciales vía e-mail u otros medios de comunicación electrónica equivalente<br><br>

                        Dado que la dirección de correo electrónico del usuario es un dato de carácter personal cuando permite identificarle, mediante su recogida en el formulario de datos online el usuario autoriza expresamente a FINQUES SASI, S.L. su tratamiento para el envío de comunicaciones comerciales o promocionales concernientes a los productos o servicios  que presta esta empresa. Estas comunicaciones estarán precedidas por la palabra publicidad al comienzo del mensaje e identificarán claramente a FINQUES SASI, S.L.. No obstante, usted puede revocar en cualquier momento su consentimiento para la recepción de comunicaciones comerciales con la simple notificación a nuestra dirección de correo electrónico (info@finques-sasi.com) o llamando al teléfono 938048171.

</p>
                    <ul class="icon-list">
                        <li>
                            <a href="https://www.google.es/maps/place/Finques+SASI/@41.5864143,1.6123442,17z/data=!4m13!1m7!3m6!1s0x12a469e114031137:0x826cdb63ff5eb73b!2sAv.+de+Barcelona,+1,+08700+Igualada,+Barcelona!3b1!8m2!3d41.5864143!4d1.6145329!3m4!1s0x12a469dcaf088c55:0xd90aff7f006a5ea7!8m2!3d41.586414!4d1.614532">
                                <i class="icon icon-LocationMarker"></i>
                                <span>Av. Barcelona, 1. 08700 Igualada (Barcelona) </span>
                            </a>
                        </li>
                        <li>
                            <a href="tel:+34938048171">
                                <i class="icon icon-Phone"></i>
                                <span>+34 938 048 171</span>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:info@finques-sasi.com">
                                <i class="icon icon-Email"></i>
                                <span>info@finques-sasi.com</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/finquessasiigualada/">
                                <i class="icon icon-Share"></i>
                                <span>facebook/finquessasiigualada</span>
                            </a>
                        </li>
                    </ul>
                </section>
  <!--              <section class="hours">
                    <header>
                        <h2>Horarios</h2>
                        <h4 class="serif">Atención al público</h4>
                    </header>
                    <p style="text-align:center">Este es el horario de todas nuestras Oficinas. </p>
                    <div class="hours">
                        <time itemprop="openingHours" datetime="Monday 10:00-24:00">
                            <strong>Lunes</strong>
                            <h3>9.30 - 13:00 / 16:30 a 20:30</h3>
                        </time>
                        <time itemprop="openingHours" datetime="Tuesday 10:00-24:00">
                            <strong>Martes</strong>
                            <h3>9.30 - 13:00 / 16:30 a 20:30</h3>
                        </time>
                        <time itemprop="openingHours" datetime="Wednesday 10:00-24:00">
                            <strong>Miércoles</strong>
                            <h3>9.30 - 13:00 / 16:30 a 20:30</h3>
                        </time>
                        <time itemprop="openingHours" datetime="Closed">
                            <strong>Jueves</strong>
                            <h3>9.30 - 13:00 / 16:30 a 20:30</h3>
                        </time>
                        <time itemprop="openingHours" datetime="Friday 10:00-24:00">
                            <strong>Viernes</strong>
                            <h3>9.30 - 13:00 / 16:30 a 20:30</h3>
                        </time>
                        <time itemprop="openingHours" datetime="Saturday 10:00-23:00">
                            <strong>Sádado</strong>
                            <h3>10.00 - 13:00 </h3>
                        </time>
                        <time itemprop="openingHours" datetime="Sunday closed">
                            <strong>Domingo</strong>
                            <h3>Cerrado</h3>
                        </time>
                    </div>
                </section>-->
                <section class="form-inline">
                    <header>
                        <h2>Contactar via e-mail</h2>
                        <h4 class="serif">Nos gusta saber de ti</h4>
                    </header>
                    <p>Si prefieres contactar a través de este formulario, rellena todas las casillas y escribe tu consulta, te contestaremos a la mayor brevedad posible. Te llamamos en menos de 24 horas.</p>
                    <form action="./php/contact.php" method="post" id="contact-form" class="form ambiance-html-form">
                        <div class="row">
                            <div class="form-group">
                                <input name="name" id="name" type="text" placeholder="nombre">
                                <i class="icon icon-User"></i>
                            </div>
                            
                            <div class="form-group">
                                <input class="full-border" name="telefon" id="telefon" type="text" placeholder="teléfono">
                                <i class="icon icon-iPad"></i>
                            </div>
                            <div class="form-group fullwidth">
                                <input class="full-border" name="email" id="email" type="email" placeholder="email">
                                <i class="icon icon-Email"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea rows="10" cols="40" required="required" name="message" id="body" placeholder="mensaje"></textarea>
                            <i class="icon icon-Typing"></i>
                        </div>
                        <span class="message"><strong>Estado.</strong> idle</span>
                        <div class="submit">
                            <button type="submit" value="submit">
                                <i class="icon icon-Forward"></i>
                            </button>
                        </div>
                    </form>
                </section>
            </section>
        </div>
        <div data-remodal-id="modal">
            <i class="icon bg icon-CommentwithLines"></i>
            <button data-remodal-action="close" class="remodal-close"></button>
            <h1>Grácias!</h1>
            <p>Nos pondremos en contancto contigo lo más pronto posible!</p>
            <div class="signature center">
                <h6>-Finques Sasi-</h6>
                <h5></h5>
            </div>
        </div>
    </main>
    <!-- end of main content -->
</div>

<!-- The slideshow -->
<ul id="slideshow" data-speed="6000">
    <li>
        <img src="<?= base_url() ?>img/slideshow/contacte.jpg" alt="slideshow image" />
    </li>
  <!-- 
  <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
 -->
</ul>
<!-- end of slideshow -->

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe.
     It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides.
        PhotoSwipe keeps only 3 of them in the DOM to save memory.
        Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>

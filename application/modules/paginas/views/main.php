
<!-- Top shadow -->
<div class="shadow"></div>
<!-- end top shadow -->

<!-- The splash screen -->
<div id="splash">
    <div class="loader">
        <img class="splash-logo" src="<?= base_url() ?>img/logo/logo.svg" />
        <div class="line"></div>
    </div>
</div>
<!-- End of splash screen -->

<div id="wrapper">
    <!-- main content -->
    <main>
        <header>
            <a href="<?= site_url() ?>" class="logo">
                <h1>Finques Sasi</h1>
                <img src="<?= base_url() ?>img/logo/logo.svg" alt="finques sasi" />
            </a>
            <div class='dir'>
            <h4>OFICINA CENTRAL</h4>
                <a href="https://www.google.es/maps/place/Finques+SASI/@41.5864143,1.6123442,17z/data=!4m13!1m7!3m6!1s0x12a469e114031137:0x826cdb63ff5eb73b!2sAv.+de+Barcelona,+1,+08700+Igualada,+Barcelona!3b1!8m2!3d41.5864143!4d1.6145329!3m4!1s0x12a469dcaf088c55:0xd90aff7f006a5ea7!8m2!3d41.586414!4d1.614532"><i class="icon icon-LocationMarker" ></i> Av. Barcelona. 1. 08700 Igualada (BCN)</i> <br></a>
                 <a href="http://www.finques-sasi.com"><i class="icon icon-Computer"></i> www.finques-sasi.com<br></a>
                <a href="mailto:info@finques-sasi.com">  <i class="icon icon-Email"></i> info@finques-sasi.com</a>
<!--                 <i class="icon icon-iPad"></i> <b>TRUCA'NS +34 938 048 171 </b> -->
            </div>
            <div class='logos'>
                <img src="<?= base_url() ?>img/logo/api.png" alt="api" /> 
             <!--    <img src="<?= base_url() ?>img/logo/api2.jpg" alt="api" /> -->
                 <div>
                    <a href="tel:+34938048171"> <i class="icon icon-iPad"style=" margin-top: 7px"></i> <b>LLÁMANOS +34 938 048 171 </b></a>
                 </div>
            </div>
        </header>
        <!-- The navigation -->
        <nav class="strokes">
            <ul id="navigation">
                <li class="logoresponsive" id="inforesponsive" style="min-height:184px;">                    
                    <section>
                        <img src="<?= base_url() ?>img/logo/logo.svg" style="width: 50%;"><br/>
                        <h4>OFICINA CENTRAL</h4>
                        <a href="https://www.google.es/maps/place/Av.+de+Barcelona,+1,+08700+Igualada,+Barcelona/@41.5864143,1.6123442,17z/data=!3m1!4b1!4m5!3m4!1s0x12a469e114031137:0x826cdb63ff5eb73b!8m2!3d41.5864143!4d1.6145329?safe=off&client=safari&rls=en&q=Av.+Barcelona.+1.+08700+Igualada+(BCN)&biw=3128&bih=1314&bav=on.2,or.r_cp.&um=1&ie=UTF-8&sa=X&ved=0ahUKEwiK9L-2pt7UAhVGKFAKHSP5CygQ_AUIBigB"><i class="icon icon-LocationMarker"></i> Av. Barcelona. 1. 08700 Igualada (BCN)</a>
                        <a href="http://www.finques-sasi.com"><i class="icon icon-Computer"></i> www.finques-sasi.com</a>
                        <a href="mailto:info@finques-sasi.com">  <i class="icon icon-Email"></i> info@finques-sasi.com</a>
                        <a href="tel:+34938048171"> <i class="icon icon-iPad"></i> <b>LLÁMANOS +34 938 048 171 </b></a>                        
                    </section>                    
                </li>
                <li class="logoresponsive">                    
                    <section>                                                
                        <img src="<?= base_url() ?>img/logo/api.png" alt="api" style="width:70px; margin-top: 57.8px;""/> 
                  <!--       <img src="<?= base_url() ?>img/logo/api2.jpg" alt="api" style="width:70px" /> -->
                    </section>                    
                </li>
                <li>
                    <a href="<?= site_url('p/empresa') ?>" data-transition="slide-to-top">
                        <section>
                            <h1>Empresa</h1>
                            <h5 class="badge-rounded">¿Quienes somos?</h5>
                        </section>
                        <footer>
                            <i class="icon icon-CommentwithLines"></i>
                            <h5 class="serif">Sobre nosotros</h5>
                            <p>Leer más</p>
                        </footer>
                    </a>
                </li>
                <li>
                    <a href="http://www.sasiigualada.com/index.php?idio=1&idio=1&limtipo=&limciudad=106199#buscador">
                        <section>
                            <h1>Barcelona </h1>
                            <h5 class="badge-rounded">Catalunya</h5>
                        </section>
                        <footer>
                            <i class="icon icon-Pin"></i>
                            <h5 class="serif">Compra - Alquiler</h5>
                            <p>Ver</p>
                        </footer>
                    </a>
                </li>
                <li>
                    <a href="http://www.sasi-alicante.com/">
                        <section>
                            <h1>Alicante</h1>
                            <h5 class="badge-rounded">País Valencià</h5>
                        </section>
                        <footer>
                            <i class="icon icon-Pin"></i>
                            <h5 class="serif">Compra - Alquiler</h5>
                            <p>Ver</p>
                        </footer>
                    </a>
                </li>
                <li>
                    <a href="http://www.sasiigualada.com/index.php" data-transition="slide-to-top">
                        <section>
                            <h1>Igualada</h1>
                            <h5 class="badge-rounded">Catalunya</h5>
                        </section>
                        <footer>
                            <i class="icon icon-Pin"></i>
                            <h5 class="serif">Compra - Alquiler</h5>
                            <p>Ver</p>
                        </footer>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('p/solicitar-trabajo') ?>" data-transition="slide-to-top">
                        <section>
                            <h1>¿Quieres colaborar</h1>
                            <h5 class="badge-rounded">con nosotros?</h5>
                        </section>
                        <footer>
                            <i class="icon icon-Users"></i>
                            <h5 class="serif">Contacta</h5>
                            <p>Rellenar formulario</p>
                        </footer>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('p/contacto') ?>" data-transition="slide-to-top">
                        <section>
                            <h1>Contacto</h1>
                            <h5 class="badge-rounded">¿Donde estamos?</h5>
                        </section>
                        <footer>
                            <i class="icon icon-LocationMarker"></i>
                            <h5 class="serif">Contacta</h5>
                            <p>Dejanos un mensaje</p>
                        </footer>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- End navigation -->
        <div class="overlay"></div>
        <div data-remodal-id="modal">
            <i class="icon bg icon-CommentwithLines"></i>
            <button data-remodal-action="close" class="remodal-close"></button>
            <h1></h1>
            <p></p>
         
        </div>
    </main>
    <!-- end of main content -->
</div>

<!-- The slideshow -->
<ul id="slideshow" data-speed="6000">
    <li>
        <img src="<?= base_url() ?>img/slideshow/1.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/2.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/3.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/4.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/5.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/6.jpg" alt="slideshow image" />
    </li>
     <li>
        <img src="<?= base_url() ?>img/slideshow/8.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/7.jpg" alt="slideshow image" />
    </li>
</ul>
<!-- end of slideshow -->

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe.
     It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides.
        PhotoSwipe keeps only 3 of them in the DOM to save memory.
        Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
<div id="barracookies">
    Usamos cookies propias y de terceros que entre otras cosas recogen datos sobre sus hábitos de navegación para mostrarle publicidad personalizada y realizar análisis de uso de nuestro sitio.
    <br/>
    Si continúa navegando consideramos que acepta su uso. 
    <a href="javascript:void(0);" onclick="var expiration = new Date(); expiration.setTime(expiration.getTime() + (60000*60*24*365)); setCookie('avisocookies','1',expiration,'/');document.getElementById('barracookies').style.display='none';"><b style="font-family: 'merriweather-italic';
">OK</b>
    </a>
    <a href="<?= site_url('p/politica') ?>" target="_blank" style="color: #fff;text-decoration: underline; margin-left: 20px;font-weight: normal;font-size: 12px;">
        Leer Política de privacidad
    </a>
</div>
<!-- Estilo barra CSS -->
<style>#barracookies {display: none;z-index: 99999;position:fixed;left:0px;right:0px;bottom:0px;width:100%;min-height:40px;font-weight: normal;padding:5px;background: #333333;color:white;line-height:20px;font-family: 'Montserrat', sans-serif; font-weight: lighter;font-size:12px;text-align:center;box-sizing:border-box;} #barracookies a:nth-child(2) {padding:4px;background:#e22013;border-radius:10px;text-decoration:none;} #barracookies a {color: #fff;text-decoration: none;}</style>
<!-- Gestión de cookies-->
<script type='text/javascript'>function setCookie(name,value,expires,path,domain,secure){document.cookie=name+"="+escape(value)+((expires==null)?"":"; expires="+expires.toGMTString())+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+((secure==null)?"":"; secure")}function getCookie(name){var cname=name+"=";var dc=document.cookie;if(dc.length>0){begin=dc.indexOf(cname);if(begin!=-1){begin+=cname.length;end=dc.indexOf(";",begin);if(end==-1)end=dc.length;return unescape(dc.substring(begin,end))}}return null}function delCookie(name,path,domain){if(getCookie(name)){document.cookie=name+"="+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+"; expires=Thu, 01-Jan-70 00:00:01 GMT"}}</script>
<!-- Gestión barra aviso cookies -->
<script type='text/javascript'>
var comprobar = getCookie("avisocookies");
if (comprobar != null) {}
else {
        var expiration = new Date();
        expiration.setTime(expiration.getTime() + (60000*60*24*365));
        setCookie("avisocookies","1",expiration);
        document.getElementById("barracookies").style.display="block"; 
        
        window.addEventListener('load',function(){
            setTimeout(function(){
                $(".edgtf-login-register-holder").toggle('modal');
            },3000);
        });    
}
</script>
    </div> <!-- close div.edgtf-wrapper-inner  -->
</div> <!-- close div.edgtf-wrapper -->
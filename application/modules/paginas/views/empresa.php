<!-- Top shadow -->
<div class="shadow"></div>
<!-- end top shadow -->

<!-- The splash screen -->
<div id="splash">
    <div class="loader">
        <img class="splash-logo" src="<?= base_url() ?>img/logo/logo.svg" />
        <div class="line"></div>
    </div>
</div>
<!-- End of splash screen -->

<div id="wrapper">
    <!-- main content -->
    <main>
        <!-- The header for content -->
        <header class="detail">
            <a href="<?= site_url() ?>" class="back" data-transition="slide-from-top">
                <h1>tornar</h1>
            </a>
            <section>
                <h3 class="badge">Quienes somos?</h3>
                <h1>EMPRESA</h1>
            </section>
        </header>
        <!-- end header -->
        <div class="content-wrap">
            <div class="content">
                <i class="icon bg icon-CommentwithLines"></i>
                <section>
                    <header>
                        <h2>Finques Sasi</h2>
                        <h4 class="serif">Igualada</h4>
                    </header>

                    <section>
                        <p>FINQUES SASI esta formada por un equipo profesional Joven y Dinámico con un objetivo común: Satisfacer las demandas de nuestros Clientes.
La sociedad tiene por objeto: la Intermediación en la Compra, Venta, Alquiler y Permuta de toda clase de Bienes Inmuebles Urbanos y Rústicos.
Una agencia joven con profesionales expertos y dinámicos. Titulación API y AICAT.</p>
                        <p>Disponemos de una amplia oferta inmobiliaria de Obra 
						Nueva, 2ª Mano, Chalets, Torres, Terrenos, Fincas 
						rústicas, Naves y Terrenos Industriales y Comerciales, 
						para Compra-Venta, Alquiler y Traspaso, todo ello con las mejores garantías económicas del mercado hasta el 100% del valor de la compra ó Gestión en la obtención de la Hipoteca en las mejores condiciones. 

</p>
                    </section>

                    <footer>
                        <div class="signature">
                            <h6>Gerente y fundador</h6>
                            <h5>Toni Giralt</h5>
                        </div>
                    </footer>
                </section>
            </div>
        </div>
        <div data-remodal-id="modal">
            <i class="icon bg icon-CommentwithLines"></i>
            <button data-remodal-action="close" class="remodal-close"></button>
            <h1></h1>
            <p></p>
            <div class="signature center">
                <h6></h6>
                <h5></h5>
            </div>
        </div>
    </main>
    <!-- end of main content -->
</div>

<!-- The slideshow -->
<ul id="slideshow" data-speed="6000">
    <li>
        <img src="<?= base_url() ?>img/slideshow/empresa.jpg" alt="slideshow image" />
    </li>
  <!-- 
  <li>
        <img src="<?= base_url() ?>img/slideshow/2.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/3.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/4.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/5.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/6.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/7.jpg" alt="slideshow image" />
    </li>
 -->
  
</ul>
<!-- end of slideshow -->

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe.
     It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides.
        PhotoSwipe keeps only 3 of them in the DOM to save memory.
        Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title><?= empty($title) ? 'Finques SASI' : $title ?></title>

        <!-- mobile setup -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="apple-mobile-web-app-title" content="Finques Sasi" />

        <!-- stylesheet -->
        <link rel="stylesheet" href="<?= base_url() ?>css/template/style-default.css">

        <!-- Page description -->
        <meta name="description" content="Fincas Sasi con multitud de propiedades, pisos , chalet , casas, villas en todas las zonas y localidades">

        <!-- Facebook -->
        <meta property="og:title" content="Finques Sasi" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content=".//" />
        <meta property="og:description" content="Fincas Sasi con multitud de propiedades, pisos , chalet , casas, villas en todas las zonas y localidades" />

        <!-- Twitter -->
        <meta name="twitter:title" content="Finques Sasi" />
        <meta name="twitter:description" content="Fincas Sasi con multitud de propiedades, pisos , chalet , casas, villas en todas las zonas y localidades" />
        <!-- Favicons -->
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>img/favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>img/favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>img/favicons/apple-touch-icon-152x152.png">
        <link rel="icon" type="image/png" href="<?= base_url() ?>img/favicons/favicon.png" sizes="96x96">

    </head>

    <body>
        <?= $this->load->view($view) ?>
        <?= $this->load->view('includes/template/scripts') ?>
    </body>
</html>
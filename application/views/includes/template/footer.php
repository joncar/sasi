<footer id="footer">
    <div class="footer-container">
        <ul class="socials">
            <li class="facebook"><a href="https://www.facebook.com/barsalus" class="circle-icon"><i class="fa fa-facebook"></i></a></li>
<!-- 
            <li class="twitter"><a href="#" class="circle-icon"><i class="fa fa-twitter"></i></a></li>
 -->
<!-- 
            <li class="flickr"><a href="#" class="circle-icon"><i class="fa fa-flickr"></i></a></li>
 -->
            <li class="youtube"><a href="https://www.youtube.com/watch?time_continue=2&v=GiBfKFJeaqA" class="circle-icon"><i class="fa fa-youtube"></i></a></li>
<!-- 
            <li class="rss"><a href="#" class="circle-icon"><i class="fa fa-rss"></i></a></li> 
 -->
<!-- 
            <li class="share-alt"><a href="#" class="circle-icon"><i class="fa fa-share-alt"></i></a></li>         
 -->
            <!--<li class="pinterest"><a href="#" class="circle-icon"><i class="fa fa-pinterest"></i></a></li>-->
            <!--<li class="github"><a href="#" class="circle-icon"><i class="fa fa-github"></i></a></li>-->
            <!--<li class="google-plus"><a href="#" class="circle-icon"><i class="fa fa-google-plus"></i></a></li>-->
            <!--<li class="linkedin"><a href="#" class="circle-icon"><i class="fa fa-linkedin"></i></a></li>-->
            <!--<li class="skype"><a href="#" class="circle-icon"><i class="fa fa-skype"></i></a></li>-->
            <!--<li class="tumblr"><a href="#" class="circle-icon"><i class="fa fa-tumblr"></i></a></li>-->                    
        </ul>	
        <h5 class="footer-copyright">© 2017 Ca'n Salus - Restaurant. Web by <a href="http://hipo.tv">hipo</a>.</h5>
    </div>
</footer>
<header id="header">
    <div id="logo" class="text-center animated" data-animation="fadeInUp" data-animation-delay="400">
        <a href="<?= site_url() ?>"><img src="<?= base_url() ?>img/logo.png" alt="Logo"></a>
    </div><!-- end logo -->
    <div id="slideshow">
        <ul class="rslides">
            <li style="background-image: url(<?= base_url() ?>img/slide_img.jpg);">
                <img src="<?= base_url() ?>img/slide_img.jpg" alt="">
                <div class="slideshow-caption">
                    <h1>Cuina<span class="highlight"> amb molt </span> gust</h1>
                    <h3>Gauideix del sabor de sempre</h3>
                    <div class="button">
                        <a href="#contact-form">Fer una reserva</a>
                    </div>	    		
                </div><!-- end .slideshow-caption -->
            </li><!-- end .slideshow item -->
            <li style="background-image: url(<?= base_url() ?>img/slide_img1.jpg);">
                <img src="<?= base_url() ?>img/slide_img1.jpg" alt="">
                <div class="slideshow-caption">
                    <h1>Cuina de<span class="highlight"> Mercat </span> i de temporada</h1>
                    <h3>Gaudeix del sabor de sempre</h3>
                    <div class="button">
                        <a href="#contact-form">Fer una reserva</a>
                    </div>	    		
                </div><!-- end .slideshow-caption -->
            </li><!-- end .slideshow item -->
            <li style="background-image: url(<?= base_url() ?>img/slide_img2.jpg);">
                <img src="<?= base_url() ?>img/slide_img2.jpg" alt="">
                <div class="slideshow-caption">
                    <h1>Amplia<span class="highlight"> carta </span> en tapes i molt més</h1>
                    <h3>Gaudeix del sabor de sempre</h3>
                    <div class="button">
                        <a href="#contact-form">Fer una reserva</a>
                    </div>	    		
                </div><!-- end .slideshow-caption -->
            </li><!-- end .slideshow item -->	  
        </ul>	
    </div><!-- end slideshow -->
</header>

<nav id="navigation">
    <div class="nav-container">	
        <ul>
            <li><a class="active" href="#welcome"><span class="extrabold">Benvinguts</span></a></li>
            <li><a href="#about">Menu <span class="extrabold"> d'avui</span></a></li>
            <li><a href="#special-gallery">Les <span class="extrabold"> Especialitats</span></a></li>            
            <li><a href="#big-menu">La <span class="extrabold"> Carta</span></a></li>
            <li><a href="#cooks">Qui <span class="extrabold">som?</span></a></li>
            <li><a href="#contact">Contacte / <span class="extrabold">Reserves</span></a></li>
            
        </ul>			
    </div><!-- end .nav-container -->
</nav>

<nav id="mobile-navigation">
    <div class="mobile-nav-container">
        <div id="menu-toggle">
            <i class="fa fa-bars"></i>
        </div>		
        <ul class="inactive">
            <li><a class="active" href="#welcome"><span class="extrabold">Benvinguts</span></a></li>
            <li><a href="#about">Menu <span class="extrabold"> d'avui</span></a></li>
            <li><a href="#special-gallery">Les <span class="extrabold"> Especialitats</span></a></li>            
            <li><a href="#big-menu">La <span class="extrabold"> Carta</span></a></li>
            <li><a href="#cooks">Qui <span class="extrabold">som?</span></a></li>
            <li><a href="#contact">>Contacte / <span class="extrabold">Reserves</span></a></li>
        </ul>			
    </div><!-- end .mobile-nav-container -->
</nav>

<a href="#" class="scrollup">
    <i class="fa fa-angle-double-up"></i>
</a><!-- end .scrollup -->